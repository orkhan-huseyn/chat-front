import React from "react";
import { useDispatch } from "react-redux";
import { Switch, Route, Redirect } from "react-router-dom";
import Home from "./pages/Home";
import Registration from "./pages/Registration";
import Login from "./pages/Login";
import { setCurrentUserAction } from "./redux/actionCreators";

import "./styles/App.css";

function App() {
  const dispatch = useDispatch();
  const accessToken = localStorage.getItem("access_token");

  if (accessToken) {
    const user = JSON.parse(localStorage.getItem("user_info"));
    dispatch(setCurrentUserAction(user));
    return (
      <Switch>
        <Route exact path="/" component={Home} />
      </Switch>
    );
  }

  return (
    <Switch>
      <Route path="/login" component={Login} />
      <Route path="/registration" component={Registration} />
      <Route render={() => <Redirect to="/login" />} />
    </Switch>
  );
}

export default App;
