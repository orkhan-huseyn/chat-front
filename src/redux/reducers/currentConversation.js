import { SET_CURRENT_CONVERSATION } from "../actionTypes";

const initialState = {
  userId: null,
};

export default function currentConversationReducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case SET_CURRENT_CONVERSATION:
      const { userId } = action.payload;
      return {
        userId,
      };
    default:
      return state;
  }
}
