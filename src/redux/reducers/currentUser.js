import { SET_CURRENT_USER } from "../actionTypes";

const initialState = {
  fullName: null,
  username: null,
  position: null,
  image: null,
};

export default function currentUserReducer(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      const { id, fullName, username, position, image } = action.payload;
      return {
        id,
        fullName,
        username,
        position,
        image,
      };
    default:
      return state;
  }
}
