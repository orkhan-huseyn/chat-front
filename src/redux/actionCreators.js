import axios from "../axios";
import {
  ADD_NEW_CONVERSATION,
  RECEIVE_MESSAGE,
  REMOVE_CONVERSATION,
  SEND_MESSAGE,
  SET_CONVERSATIONS,
  SET_CURRENT_CONVERSATION,
  SET_CURRENT_USER,
  SET_USER_MESSAGES,
} from "./actionTypes";

export function setCurrentUserAction(user) {
  return {
    type: SET_CURRENT_USER,
    payload: user,
  };
}

export function setCurrentConversationAction(userId) {
  return {
    type: SET_CURRENT_CONVERSATION,
    payload: {
      userId,
    },
  };
}

export function addNewConversationAction(user) {
  return {
    type: ADD_NEW_CONVERSATION,
    payload: user,
  };
}

export function removeConversationAction(id) {
  return {
    type: REMOVE_CONVERSATION,
    payload: id,
  };
}

export function fetchConversations() {
  return async (dispatch) => {
    const { data } = await axios.get("users");
    dispatch(setConversationsAction(data.payload));
  };
}

export function setConversationsAction(conversations) {
  return {
    type: SET_CONVERSATIONS,
    payload: conversations,
  };
}

export function postMessage(toUserId, content) {
  return async (dispatch) => {
    await axios.post(`users/${toUserId}/messages`, { content });
    dispatch(sendMessageAction(toUserId, content));
  };
}

export function fetchMessages(userId) {
  return async (dispatch) => {
    const { data } = await axios.get(`users/${userId}/messages`);
    dispatch(setUserMessages(userId, data.payload));
  };
}

export function setUserMessages(userId, messages) {
  return {
    type: SET_USER_MESSAGES,
    payload: {
      userId,
      messages,
    },
  };
}

export function sendMessageAction(toUserId, content) {
  return {
    type: SEND_MESSAGE,
    payload: {
      toUserId,
      content,
    },
  };
}

export function receiveMessageAction(fromUserId, content) {
  return {
    type: RECEIVE_MESSAGE,
    payload: {
      fromUserId,
      content,
    },
  };
}
