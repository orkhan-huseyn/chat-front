import { useFormik } from "formik";
import { useState } from "react";
import { Link } from "react-router-dom";
import axios from "../axios";

import "./../styles/Join.css";

function Login() {
  const [showPassword, setShowPassword] = useState(false);

  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    validate: (values) => {
      const errors = {};
      if (values.username === "") {
        errors.username = "You must enter your username";
      }
      if (values.password === "") {
        errors.password = "You must enter your password";
      }
      return errors;
    },
    onSubmit: async (values, { setSubmitting }) => {
      const { data } = await axios.post("login", values);
      setSubmitting(false);
      if (!data.error) {
        localStorage.setItem("access_token", data.payload.accessToken);
        localStorage.setItem("user_info", JSON.stringify(data.payload.user));
        window.location.href = "/";
      } else {
        // TODO: show message
      }
    },
  });

  return (
    <form
      data-testid="registration-component"
      className="registration"
      autoComplete="off"
      onSubmit={formik.handleSubmit}
    >
      <div className="form">
        <h1>Login</h1>
        <div className="username">
          <input
            placeholder="Enter your username"
            aria-label="Enter your username"
            name="username"
            value={formik.values.username}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.touched.username && formik.errors.username ? (
            <small>{formik.errors.username}</small>
          ) : null}
        </div>
        <div className="username position">
          <div className="input-group">
            <input
              placeholder="Enter your password"
              aria-label="Enter your password"
              name="password"
              type={showPassword ? "text" : "password"}
              value={formik.values.password}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            <button
              type="button"
              onClick={() => setShowPassword(!showPassword)}
            >
              {showPassword ? "hide" : "show"}
            </button>
          </div>
          {formik.touched.password && formik.errors.password ? (
            <small>{formik.errors.password}</small>
          ) : null}
        </div>
        <button
          type="submit"
          disabled={formik.isSubmitting || !formik.isValid}
          className="submit"
        >
          {formik.isSubmitting ? "Joininig chat..." : "Join chat"}
        </button>
        <Link style={{ display: "block" }} to="/registration">
          Register instead
        </Link>
      </div>
    </form>
  );
}

export default Login;
