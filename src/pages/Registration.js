import { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import axios from "../axios";

import "./../styles/Join.css";

function Registration() {
  const history = useHistory();
  const [fullName, setFullName] = useState("");
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [position, setPosition] = useState("");
  const [image, setImage] = useState("");
  const [showPassword, setShowPassword] = useState(false);

  async function handleUsernameSubmit(event) {
    event.preventDefault();
    await axios.post("registration", {
      fullName,
      username,
      position,
      image,
      password,
    });
    setTimeout(() => {
      history.push("/login");
    }, 2000);
  }

  async function handleFileChange(event) {
    const file = event.target.files[0];
    const formData = new FormData();
    formData.append("avatar", file);
    const { data } = await axios.post("upload", formData);
    setImage(data.payload);
  }

  return (
    <form
      data-testid="registration-component"
      className="registration"
      autoComplete="off"
      onSubmit={handleUsernameSubmit}
    >
      <div className="form">
        <h1>Registration</h1>
        <div className="username">
          <input
            placeholder="Enter your full name"
            aria-label="Enter your full name"
            value={fullName}
            onChange={(e) => setFullName(e.target.value)}
          />
        </div>
        <div className="username">
          <input
            placeholder="Enter your username"
            aria-label="Enter your username"
            value={username}
            onChange={(e) => setUserName(e.target.value)}
          />
        </div>
        <div className="username position">
          <input
            placeholder="Enter your position/role"
            aria-label="Enter your position/role"
            value={position}
            onChange={(e) => setPosition(e.target.value)}
          />
        </div>
        <div className="username position">
          <div className="input-group">
            <input
              placeholder="Enter your password"
              aria-label="Enter your password"
              type={showPassword ? "text" : "password"}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <button
              type="button"
              onClick={() => setShowPassword(!showPassword)}
            >
              {showPassword ? "hide" : "show"}
            </button>
          </div>
        </div>
        <div className="username position">
          <input
            type="file"
            aria-label="Select your avatar image"
            onChange={handleFileChange}
          />
        </div>
        <button className="submit">Register</button>
        <Link style={{ display: "block" }} to="/login">
          Login instead
        </Link>
      </div>
    </form>
  );
}

export default Registration;
