import React, { useEffect } from "react";
import {
  fetchMessages,
  receiveMessageAction,
  setCurrentConversationAction,
} from "../redux/actionCreators";
import { useDispatch, useSelector } from "react-redux";

import Chat from "../components/Chat";
import Profile from "../components/Profile";
import socket from "../socket";
import styled from "styled-components";

function Home() {
  const currentUser = useSelector((state) => state.currentUser);
  const currentConversation = useSelector((state) => state.currentConversation);
  const messages = useSelector(
    (state) => state.messages[currentConversation.userId]
  );
  const dispatch = useDispatch();

  useEffect(() => {
    socket.auth = { userId: currentUser.id };
    socket.connect();

    socket.on("receive message", (message, userId) => {
      dispatch(receiveMessageAction(userId, message));
    });

    return () => {
      socket.off("receive message");
    };
    // eslint-disable-next-line
  }, []);

  const handleConversationClick = (userId) => {
    dispatch(fetchMessages(userId));
    dispatch(setCurrentConversationAction(userId));
  };

  return (
    <Page data-testid="home-component">
      <Box>
        <Profile onConversationClick={handleConversationClick} />
        {currentConversation.userId ? (
          <Chat toUserId={currentConversation.userId} messages={messages} />
        ) : (
          <span>Select a conversation to start chatting</span>
        )}
      </Box>
    </Page>
  );
}

const Page = styled.div`
  display: flex;
  height: 100vh;
  align-items: center;
  justify-content: center;
  background-color: white;
`;

const Box = styled.div`
  height: 90%;
  width: 90%;
  background-color: white;
  border-radius: 20px;
  display: flex;
`;

export default Home;
